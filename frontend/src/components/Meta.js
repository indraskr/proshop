import {Helmet} from 'react-helmet';

const Meta = ({title, desc, keywords}) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta name='description' content={desc} />
      <meta name='keywords' content={keywords} />
    </Helmet>
  );
};

Meta.defaultProps = {
  title: 'Welcome to ProShop',
  desc: 'We sell best products',
  keywords: 'electronics,cats,cheap',
};

export default Meta;
