import {Col, Container, Row} from 'react-bootstrap';

const Footer = () => {
  return (
    <Container>
      <Row>
        <Col className='text-center py-3'>Copyright &copy; Pro Shop</Col>
      </Row>
    </Container>
  );
};

export default Footer;
