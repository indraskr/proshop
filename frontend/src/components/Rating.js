const Rating = ({value, text, color}) => {
  return (
    <div className='rating'>
      <span>
        <i
          style={{color}}
          className={
            value >= 1
              ? 'ri-star-fill'
              : value >= 0.5
              ? 'ri-star-half-line'
              : 'ri-star-line'
          }></i>
      </span>
      <span>
        <i
          style={{color}}
          className={
            value >= 2
              ? 'ri-star-fill'
              : value >= 1.5
              ? 'ri-star-half-line'
              : 'ri-star-line'
          }></i>
      </span>
      <span>
        <i
          style={{color}}
          className={
            value >= 3
              ? 'ri-star-fill'
              : value >= 2.5
              ? 'ri-star-half-line'
              : 'ri-star-line'
          }></i>
      </span>
      <span>
        <i
          style={{color}}
          className={
            value >= 4
              ? 'ri-star-fill'
              : value >= 3.5
              ? 'ri-star-half-line'
              : 'ri-star-line'
          }></i>
      </span>
      <span>
        <i
          style={{color}}
          className={
            value >= 5
              ? 'ri-star-fill'
              : value >= 4.5
              ? 'ri-star-half-line'
              : 'ri-star-line'
          }></i>
      </span>
      <span>{text && text}</span>
    </div>
  );
};

Rating.defaultProps = {
  color: 'orangered',
};

export default Rating;
