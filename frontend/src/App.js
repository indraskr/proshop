import Footer from 'components/Footer';
import Header from 'components/Header';
import {Container} from 'react-bootstrap';
import HomeScreen from 'Screens/HomeScreen';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import ProductScreen from 'Screens/ProductScreen';
import CartScreen from 'Screens/CartScreen';
import LoginScreen from 'Screens/LoginScreen';
import RegisterScreen from 'Screens/RegisterScreen';
import ProfileScreen from 'Screens/ProfileScreen';
import ShippingScreen from 'Screens/ShippingScreen';
import PaymentScreen from 'Screens/PaymentScreen';
import PlaceOrderScreen from 'Screens/PlaceOrderScreen';
import OrderScreen from 'Screens/OrderScreen';
import UserListScreen from 'Screens/UserListScreen';
import UserEditScreen from 'Screens/UserEditScreen';
import ProductListScreen from 'Screens/ProductListScreen';
import ProductEditScreen from 'Screens/ProductEditScreen';
import OrderListScreen from 'Screens/OrderListScreen';

const App = () => {
  return (
    <Router>
      <Header />
      <main className='py-3'>
        <Container>
          <Switch>
            <Route exact path='/login' component={LoginScreen} />
            <Route exact path='/register' component={RegisterScreen} />
            <Route exact path='/profile' component={ProfileScreen} />
            <Route exact path='/shipping' component={ShippingScreen} />
            <Route exact path='/payment' component={PaymentScreen} />
            <Route exact path='/placeOrder' component={PlaceOrderScreen} />
            <Route exact path='/order/:id' component={OrderScreen} />
            <Route exact path='/product/:id' component={ProductScreen} />
            <Route exact path='/cart/:id?' component={CartScreen} />
            <Route exact path='/admin/userList' component={UserListScreen} />
            <Route exact path='/admin/orderList' component={OrderListScreen} />
            <Route
              exact
              path='/admin/productList'
              component={ProductListScreen}
            />
            <Route
              exact
              path='/admin/productList/:pageNumber'
              component={ProductListScreen}
            />
            <Route
              exact
              path='/admin/user/:id/edit'
              component={UserEditScreen}
            />
            <Route
              exact
              path='/admin/product/:id/edit'
              component={ProductEditScreen}
            />
            <Route exact path='/' component={HomeScreen} />
            <Route path='/search/:keyword' component={HomeScreen} exact />
            <Route path='/page/:pageNumber' component={HomeScreen} exact />
            <Route
              path='/search/:keyword/page/:pageNumber'
              component={HomeScreen}
              exact
            />
          </Switch>
        </Container>
      </main>
      <Footer />
    </Router>
  );
};

export default App;
